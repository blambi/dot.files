function jmp() {
    if [[ $# -gt 0 ]]; then
        eval pushd $(find $1 -type d | fzf)
    else
        eval jmp $(fzf < ~/.jmp_folders)
    fi
}
