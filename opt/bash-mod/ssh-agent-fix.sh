#!/bin/bash

if [ -f /tmp/sshinfo.sh ]; then
    if [ "$SSH_CLIENT" != "" ]; then
        # We can rely on our os doing it for locals at least..
        source /tmp/sshinfo.sh
        echo "Found ssh-agent ($SSH_AGENT_PID)" > /dev/stderr
    fi
else
    echo "export SSH_AUTH_SOCK='$SSH_AUTH_SOCK'
    export SSH_AGENT_PID='$SSH_AGENT_PID'" > /tmp/sshinfo.sh
fi
