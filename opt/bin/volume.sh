#!/bin/bash

NUM="${2:-5}"
PULSE=true

if [ "$PULSE" = true ]; then
    function up { pulsemixer --change-volume "+$NUM" --unmute ; }
    function down { pulsemixer --change-volume "-$NUM" --unmute ; }
    function mute { pulsemixer --mute ; }
    function toggle { pulsemixer --toggle-mute ; }
    function get_volume { pulsemixer --get-volume | cut -d ' ' -f 1 ; }
else
    CARD=`aplay -l | grep "ALC1220 Analog" | awk '{print substr($2,1,1)}'`

    function up { amixer -c $CARD set Master "$NUM%+" unmute ; }
    function down { amixer -c $CARD set Master "$NUM%-" unmute ; }
    function mute { amixer -c $CARD sset Master mute ; }
    function toggle { amixer -c $CARD sset Master toggle ; }
    function get_volume {
        amixer -c $CARD get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1
    }
fi

function send_notification {
    volume=$(get_volume)
    notify-send -i audio-volume-muted-blocking -t 550 -u normal "Volume" "$volume%"
}

case $1 in
    up)
        up
	send_notification
	;;
    down)
        down
	send_notification
	;;
    mute)
        mute
        ;;
    toggle)
        toggle
        ;;
    *)
        echo "Default device: $(get_volume)%"
        ;;
esac

